package pcs.lab04.sem;

import java.util.concurrent.*;

public class AgentB extends Thread {
	private Semaphore ev;
	
	public AgentB(Semaphore ev){
		this.ev = ev;
	}
	
	public void run(){
		try {
			ev.acquire();
			for (int i = 0; i < 5; i++){
				System.out.println("World");
				try {
					Thread.sleep(100);
				} catch (Exception ex){}
			}
		} catch (Exception ex){
			
		}
	}

}
