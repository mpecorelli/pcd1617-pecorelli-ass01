package pcs.lab04.sem;

import java.util.concurrent.Semaphore;

public class TestSynch {

	public static void main(String[] args) {
		Semaphore ev = new Semaphore(0);
		new AgentA(ev).start();
		new AgentB(ev).start();
	}

}
