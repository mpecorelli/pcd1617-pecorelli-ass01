package pcd.ass01.ex2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by margherita on 30/03/17.
 */
public class View extends JFrame implements ActionListener {

    private Controller controller;

    public View(final Controller controller){
        super();
        setSize(400, 100);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        JPanel panel = new JPanel();

        JButton start = new JButton("start");
        start.addActionListener(this);

        JButton stop = new JButton("stop");
        stop.addActionListener(this);

        panel.add(start);
        panel.add(stop);
        setContentPane(panel);
        this.controller = controller;
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        if (actionEvent.getActionCommand().equals("start")) {
            try {
                controller.start();
            } catch (Exception ex) {
            }
        } else {
            try {
                this.controller.stop();
            } catch (InterruptedException e) {
            }
            System.exit(0);
        }
    }
}
