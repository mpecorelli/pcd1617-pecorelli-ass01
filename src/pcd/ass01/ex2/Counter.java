package pcd.ass01.ex2;

/**
 * Created by margherita on 30/03/17.
 */
public class Counter {

    private int counter = 0;

    public void increment() {
        this.counter++;
    }

    public int getCounter() {
        return this.counter;
    }

}
