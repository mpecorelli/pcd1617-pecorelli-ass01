package pcd.ass01.ex2;

import java.util.concurrent.Semaphore;

/**
 * Created by margherita on 30/03/17.
 */
public class Controller {

    private Counter counter = new Counter();
    private Semaphore semaphorePingPong = new Semaphore(1);
    private Semaphore semaphoreViewer = new Semaphore(1);
    private Semaphore semaphorePrint = new Semaphore(1);
    private Pinger ping = new Pinger(counter, semaphorePingPong, semaphoreViewer, semaphorePrint);
    private Ponger pong = new Ponger(counter, semaphorePingPong, semaphoreViewer, semaphorePrint);
    private Viewer viewer = new Viewer(counter, semaphoreViewer, semaphorePrint);

    public void start() {
        this.ping.start();
        this.viewer.start();
        this.pong.start();
    }

    public void stop() throws InterruptedException {
        this.ping.setGoOn(false);
        this.pong.setGoOn(false);
        try {
            this.ping.join();
            this.pong.join();
        } catch (Exception ex) {
        }
        this.viewer.setGoOn(false);
        try {
            this.viewer.join();
        } catch (Exception ex) {
        }
    }
}
