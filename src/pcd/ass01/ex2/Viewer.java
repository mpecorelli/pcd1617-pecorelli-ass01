package pcd.ass01.ex2;

import java.util.concurrent.Semaphore;

/**
 * Created by margherita on 30/03/17.
 */
public class Viewer extends Thread {

    private Counter counter;
    private Semaphore semaphoreViewer;
    private Semaphore semaphorePrint;
    private boolean goOn = true;
    private boolean isPing = true;

    public Viewer(final Counter counter, final Semaphore semaphoreViewer, final Semaphore semaphorePrint) {
        this.counter = counter;
        this.semaphoreViewer = semaphoreViewer;
        this.semaphorePrint = semaphorePrint;
    }

    public void setGoOn(final boolean goOn) {
        this.goOn = goOn;
    }

    @Override
    public void run() {
        try {
            semaphorePrint.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        while (goOn) {
            try {
                semaphoreViewer.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(counter.getCounter());
            semaphorePrint.release();

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        semaphorePrint.release();
        System.out.println("Viewer: Goodbye");
    }

    public static void main(String[] args) throws Exception {
		View view = new View(new Controller());
		view.setVisible(true);

    }

}
