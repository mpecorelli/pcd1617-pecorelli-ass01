package pcd.ass01.ex2;

import java.util.concurrent.Semaphore;

/**
 * Created by margherita on 30/03/17.
 */
public class Pinger extends Thread {

    private Counter counter;
    private Semaphore semaphorePingPong;
    private Semaphore semaphoreViewer;
    private Semaphore semaphorePrint;
    private boolean goOn = true;

    public Pinger(final Counter counter, final Semaphore semaphorePingPong, final Semaphore semaphoreViewer, final Semaphore semaphorePrint) {
        this.counter = counter;
        this.semaphorePingPong = semaphorePingPong;
        this.semaphoreViewer = semaphoreViewer;
        this.semaphorePrint = semaphorePrint;
    }

    public void setGoOn(final boolean goOn) {
        this.goOn = goOn;
    }

    @Override
    public void run() {
        semaphoreViewer.release();
        while (goOn) {
            try {
                semaphorePingPong.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                this.semaphorePrint.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("ping");
            counter.increment();
            semaphoreViewer.release();
            semaphorePingPong.release();

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Pinger: Goodbye");
    }

}
