package pcd.ass01.ex1;

import java.util.Optional;

/**
 * Created by margherita on 30/03/17.
 */
public class Worker extends Thread {

    private final int w;
    private final int h;
    private final int nIterMax;
    private final MandelbrotSetImageConcurImpl mand;
    private final Complex center;
    private final double delta;

    private int x;
    private int y;

    private boolean goOn = true;

    public Worker(final int w, final int h, final int nIterMax, final MandelbrotSetImageConcurImpl mand, final Complex center, final double delta) {
        this.w = w;
        this.h = h;
        this.nIterMax = nIterMax;
        this.mand = mand;
        this.center = center;
        this. delta = delta;
    }

    @Override
    public void run() {
        while (goOn) {
            Optional op = mand.nextPoint(this);
            if(op.isPresent()) {
                Complex complex = (Complex) op.get();
                double level = computeColor(complex,nIterMax);
                int color = (int)(level*255);
                mand.setImage(this.x, this.y, color + (color << 8)+ (color << 16));
            } else {
                //System.out.println("fatto");
                goOn = false;
            }
        }
    }

    private double computeColor(final Complex complex, final int maxIteration){
        int iteration = 0;
        Complex z = new Complex(0,0);
        while(z.absFast() <= 2 &&  iteration < maxIteration) {
            z = z.times(z).plus(complex);
            iteration++;
        }
        if(iteration == maxIteration) {
            return 0;
        } else {
            return 1.0-((double)iteration)/maxIteration;
        }
    }

    public void setX(final int x) {
        this.x = x;
    }

    public void setY(final int y) {
        this.y = y;
    }

}
