package pcd.ass01.ex1;

/**
 * Created by margherita on 30/03/17.
 */
public class ConcurrentMandelbrotViewer {

    public static void main(String[] args) throws Exception {
        int width = 4000;
        int height = 4000;
        int nIter = 500;

        Complex c0 = new Complex(-0.75,0);
        double rad0 = 2;

		/*
		Complex c1 = new Complex(-0.75,0.1);
		double rad1 = 0.02;

		Complex c2 = new Complex(0.7485,0.0505);
		double rad2 = 0.000002;

		Complex c3 = new Complex(0.254,0);
		double rad3 = 0.001;

		*/

		/* creating the set */
        MandelbrotSetImageConcurImpl set = new MandelbrotSetImageConcurImpl(width,height, c0, rad0);

        System.out.println("Computing...");
        StopWatch cron = new StopWatch();
        cron.start();

		/* computing the image */
        set.compute(nIter);
        cron.stop();
        System.out.println("done - "+cron.getTime()+" ms");

		/* showing the image */
        MandelbrotView view = new MandelbrotView(set,800,600);
        view.setVisible(true);


    }

}
