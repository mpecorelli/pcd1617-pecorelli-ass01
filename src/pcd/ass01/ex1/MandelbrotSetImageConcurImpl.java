package pcd.ass01.ex1;

import java.util.Optional;

/**
 * Created by margherita on 30/03/17.
 */
public class MandelbrotSetImageConcurImpl implements MandelbrotSetImage {

    private final int w;
    private final int h;
    private final int image[];
    private final Complex center;
    private final double delta;

    private int x = 0;
    private int y = 0;

    public MandelbrotSetImageConcurImpl(final int w, final int h, final Complex c, final double radius){
        this.w = w;
        this.h = h;
        image = new int[w*h];
        center = c;
        delta = radius/(w*0.5);
    }

    @Override
    public void compute(int nIterMax) {
        Worker[] workers = new Worker[Runtime.getRuntime().availableProcessors()+1];
        try {
            for (int i = 0; i < workers.length; i++){
                workers[i] = new Worker(w,h,nIterMax,this, center, delta);
                workers[i].start();
            }
            for (Worker w: workers){
                w.join();
            }
        } catch (Exception ex){
            System.out.println(ex);
        }
    }

    public synchronized Optional<Complex> nextPoint(final Worker worker) {
        if(this.y < this.h) {
            //System.out.println("x "+x+" ; y " +y);
            worker.setX(x);
            worker.setY(y);
            return Optional.of(getPoint(this.x,this.y));
        } else if(this.x < this.w - 1) {
            this.y = 0;
            this.x++;
            return nextPoint(worker);
        } else {
            //System.out.println("x "+x+" ; y " +y);
            return Optional.empty();
        }
    }

    @Override
    public Complex getPoint(final int x, final int y){
        this.y++;
        return new Complex((x - w*0.5)*delta + center.re(), center.im() - (y - h*0.5)*delta);
    }

    @Override
    public int getHeight() {
        return h;
    }

    @Override
    public int getWidth() {
        return w;
    }

    @Override
    public int[] getImage() {
        return image;
    }

    public synchronized void setImage(final int x, final int y, final int color) {
        this.image[y*w+x] = color;
    }
}